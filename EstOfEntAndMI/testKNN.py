# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 18:27:33 2015

@author: xiaolin
"""

from knn import *
from GaussApprox import *
import numpy as np
import scipy.stats


# test knn

a = np.array([5.,6.])
b = np.array([1.,1.5,3.])
c = knn(a,b,2,0,1)

# test entropy, draw samples from a known Gaussian distribution, compute entropy using both methods

numOfSamples = 5000

samples_1 = np.random.randn(numOfSamples) 

mu = np.array([1, 10])
Sigma = np.array([[0.1, 0],[0, 0.01]])
samples_2 = np.random.multivariate_normal(mu, Sigma, numOfSamples)

entGauss = ENT_GaussApprox(samples_2, 2.)

entKNN = knnENT(samples_2, 4., 0., 2.)


samples_1 += 10
entScipy = scipy.stats.entropy(samples_1.T) 

# test mutual information, draw samples from a known  joint Gaussian distribution, compute MI using both methods
numOfSamples = 50000
mu = np.array([1., 1.])
Sigma = np.array([[0.01, 0.005],[0.005, 0.01]])
samples_2 = np.random.multivariate_normal(mu, Sigma, numOfSamples)

MIGauss = MI_GaussApprox(samples_2, [1., 1.])
MIKnn = knnMI(samples_2, 5, [1., 1.])

