# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 16:50:40 2015

@author: xiaolin
"""
import numpy as np


#---------------------------------------------------------------------------
# Compute entropy using Gaussian approximation    
#
# n dimentional Gassian distribution : x ~ N(mu, sigma) ; sigma is covariance matrix
# p(x) = ( (2pi)^n*det(sigma) )^(-1/2)* exp(-1/2 * (x - mu).T *sigma^(-1)* (x - mu))
#
# Entropy: H(x) = 1/2*log( (2*pi*e)^n * det(sigma))
#---------------------------------------------------------------------------  
def ENT_GaussApprox(my_chain, my_d):
        
    my_Sigma = np.cov(my_chain.T)

    # Estimate entropy
    if my_d == 1:
        exactENT = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d * my_Sigma ))
    else:   
        exactENT = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d * np.linalg.det(my_Sigma) ))
        
    ent = exactENT
    return ent
    

#---------------------------------------------------------------------------
# Compute mutual information using Gaussian approximation  
#
# I(X;Y) = 1/2*log( det(sigma_x)*det(sigma_y) / det(sigma) )
# 
# sigma = [ sigma_x     sigma_xy
#           sigma_yx    sigma_y ]
#---------------------------------------------------------------------------
def MI_GaussApprox(my_chain,my_split):
    [lenChain, dimChain] = my_chain.shape
    my_dx = my_split[0]
    #my_dy = my_split[1]
    my_d = sum(my_split)

    if( my_d != dimChain ):
        print('ERROR: the joint dimension in the split is different from the dimension of the chain')
        import sys
        sys.exit(1)

    my_Sigma = np.cov(my_chain.T)

    # Estimate mutual information

    my_Sigma_x = my_Sigma[0:my_dx][:,0:my_dx]
    my_Sigma_y = my_Sigma[my_dx:][:,my_dx:]
    
    mi = 0.5*np.log(np.linalg.det(my_Sigma_x)*np.linalg.det(my_Sigma_y)/np.linalg.det(my_Sigma))
    
    return mi
'''    
#---------------------------------------------------------------------------
# Compute mutual information using Gaussian approximation    
#---------------------------------------------------------------------------    
def MI_GaussApproxOld(my_chain,my_split):

    [lenChain, dimChain] = my_chain.shape
    my_dx = my_split[0]
    my_dy = my_split[1]
    my_d = sum(my_split)

    if( my_d != dimChain ):
        print('ERROR: the joint dimension in the split is different from the dimension of the chain')
        import sys
        sys.exit(1)

    my_Sigma = np.cov(my_chain.T)

    # Estimate mutual information

    my_Sigma_x = my_Sigma[0:my_dx][:,0:my_dx]
    my_Sigma_y = my_Sigma[my_dx:][:,my_dx:]
    (sign_x, logdet_x) = np.linalg.slogdet(my_Sigma_x) 
    (sign_y, logdet_y) = np.linalg.slogdet(my_Sigma_y) 
    (sign, logdet) = np.linalg.slogdet(my_Sigma) 
    exactENT_x = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_dx  ) ) + logdet_x
    exactENT_y = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_dy ) ) + logdet_y
    exactENT_xy = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  ) ) + logdet

    exactMI = exactENT_x + exactENT_y - exactENT_xy

    mi = exactMI
    return mi

'''