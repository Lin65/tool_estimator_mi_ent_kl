import numpy as np
import copy
import scipy.special 
import sys
#--------------------------------------------------------------------------
# Program to find the k - nearest neighbors (kNN) within a set of points. 
# 
# Usage:
# [neighbors distances] = kNearestNeighbors(queryMatrix, dataMatrix, k, distType,dim);
# queryMatrix (M x D) - M query vectors with dimensionality D
# dataMatrix  (N x D) - N vectors with dimensionality D (within which we search for the nearest neighbors)
# k           (1 x 1) - Number of nearest neighbors desired
# distType    (0 or 1)- 0 indicates maximum norm, 1 means Euclidean distance
# dim         (1 x 1) - dimensionality D of queryMatrix and dataMatrix
#
# Example:
# a = [1 1; 2 2; 3 2; 4 4; 5 6];
# b = [1 1; 2 1; 6 2];
# distances = kNearestNeighbors(b,a,2,0,2);
# 
# Output:
# 
# distances =
#          0    1.4142
#     1.0000    1.0000
#     2.8284    3.0000
#--------------------------------------------------------------------------
def knn(queryMatrix, dataMatrix, k, distType, dim):

    neighborDist = np.zeros((queryMatrix.shape[0], k))
    
    numDataVectors = dataMatrix.shape[0]
    numQueryVectors = queryMatrix.shape[0]
#    print numQueryVectors
    
    if (dim > 1):
        for i in range(0, numQueryVectors):
        
            diffMat = np.tile(queryMatrix[i,:], (numDataVectors, 1)) - dataMatrix;
        
            if (distType == 0):
                dist = np.amax(abs(diffMat), axis = 1)
            else:
                dist = np.sqrt(np.sum(diffMat**2, axis = 1))
        
            sortedDist = np.sort(dist, kind='mergesort')
            neighborDist[i,:] = sortedDist[0:k]           
    else:
        for i in range(0, numQueryVectors):
        
            diffMat = np.tile(queryMatrix[i], (1,numDataVectors))[0] - dataMatrix;
        
            if (distType == 0):
                dist = abs(diffMat)
            else:
                dist = abs(diffMat)
        
            sortedDist = np.sort(dist, kind='mergesort')
            neighborDist[i,:] = sortedDist[0:k]            
     
    return neighborDist
    
#--------------------------------------------------------------------------
# Program returns the number of points within my_dist
#--------------------------------------------------------------------------        
def knnDist(queryMatrix, dataMatrix, my_dists, distType):
    
    numPts = np.zeros(queryMatrix.shape[0])
    
    numDataVectors = dataMatrix.shape[0]
    numQueryVectors = queryMatrix.shape[0]
        
    for i in range(0, numQueryVectors):
        
        diffMat = np.tile(queryMatrix[i,:], (numDataVectors, 1)) - dataMatrix;
        
        
        if (distType == 0):
            dist = np.amax(abs(diffMat), axis = 1)
        else:
            dist = np.sqrt(np.sum(diffMat**2, axis = 1))
        
        numPts[i] = np.size(np.where(dist <= my_dists[i]))
    
    return numPts

#--------------------------------------------------------------------------
# Program to compute entropy using KNN method 
#--------------------------------------------------------------------------        
def knnENT(my_chain, my_knn, distType, dim):
    
    lenChain = my_chain.shape[0]
    dimChain = dim
    distToK = knn(my_chain, my_chain, my_knn+1, distType, dim) 
    
    distOfK = distToK[:,my_knn]
    
 #   for i in range(0, lenChain):
 #       if (distOfK[i]==0):
 #           distOfK[i] = 1
    
    logDist = np.log(2*distOfK)
    
    ent = - scipy.special.psi(my_knn) + scipy.special.psi(lenChain) + dimChain/lenChain * np.sum(logDist)
    
    return ent

#--------------------------------------------------------------------------
# Program to compute mutual information using KNN method 
#--------------------------------------------------------------------------        
def knnMI(my_chain, my_knn, my_split):
    
    (lenChain, dimChain) = my_chain.shape
    my_dx = my_split[0]
    dim = my_split[0] + my_split[1]

    if(dim != dimChain):
        print "the joint dimension in the split is different from the dimension of the chain"
        sys.exit(1)
    
    # Normalize the chain
    mean_chain = np.mean(my_chain, axis = 0)
    std_chain = np.std(my_chain, axis = 0)
    
    my_chain = (my_chain - np.tile(mean_chain, (lenChain, 1)))/np.tile(std_chain, (lenChain, 1))
    
    # Estimate mutual information
    distToK = knn(my_chain, my_chain, my_knn+1, 0, dimChain)
    distOfK = distToK[:,my_knn]
    
    numPts_x = knnDist(my_chain[:,0:my_dx], my_chain[:,0:my_dx], distOfK, 0)
    numPts_y = knnDist(my_chain[:,my_dx:], my_chain[:,my_dx:], distOfK, 0)
    
    mi = scipy.special.psi(my_knn) + scipy.special.psi(lenChain) - np.mean(scipy.special.psi(numPts_x + 1) + scipy.special.psi(numPts_y + 1))
      
    return mi        
   

        
    
    
        




















  