function [mi,k] = knnMaxMI(my_chain,knn_min,knn_max,my_split)

knn_vec = knn_min:knn_max;
mi_vec = zeros(size(knn_vec));

for ind_knn = 1:length(knn_vec)

    my_knn = knn_vec(ind_knn);
    mi_vec(ind_knn) = knnMI(my_chain,my_knn,my_split);

end;

[mi,ii] = max(mi_vec);
k = knn_vec(ii);