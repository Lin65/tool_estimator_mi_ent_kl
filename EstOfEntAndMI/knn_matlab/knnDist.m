function noPts = knnDist(dataMatrix, queryMatrix, my_dists, distType)
%--------------------------------------------------------------------------
% Program returns the number of points within my_dist
%--------------------------------------------------------------------------

noPts = zeros(size(queryMatrix,1),1);

numDataVectors = size(dataMatrix,1);
numQueryVectors = size(queryMatrix,1);

for i=1:numQueryVectors,
    
    diffMat = repmat(queryMatrix(i,:),numDataVectors,1)-dataMatrix;
       
    if isinf(distType),
        % Linf - dist
        dist = max(abs(diffMat),[],2);
    else
        % L2 - dist
        dist = sqrt(sum(diffMat.^2,2));
    end;

    noPts(i) = length(find(dist <= my_dists(i)));

end