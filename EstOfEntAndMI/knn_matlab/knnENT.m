function ent = knnENT(my_chain,my_knn)

[lenChain, dimChain] = size(my_chain);
[nn,dd] = knn(my_chain, my_chain, my_knn, inf);    

log_dist = log(2*dd(:,end));
log_dist(isinf(log_dist)) = 0;
ent = - psi(my_knn) + psi(lenChain) + dimChain/lenChain * sum(log_dist);
